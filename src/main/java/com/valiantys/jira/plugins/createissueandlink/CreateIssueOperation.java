package com.valiantys.jira.plugins.createissueandlink;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Category;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.issueoperation.AbstractPluggableIssueOperation;
import com.atlassian.jira.plugin.issueoperation.IssueOperationModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.opensymphony.user.User;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping;
import com.valiantys.jira.plugins.createissueandlink.util.LinkedIssueConstants;

/**
 * This operation creates automatically a new linked issue. the
 * contextualization of the creation is done in the xml file.
 * 
 * @author Maxime
 * @since v1.0.0
 */
public class CreateIssueOperation extends AbstractPluggableIssueOperation {

	/**
	 * Loggeur.
	 */
	private static final Category LOG = Category
			.getInstance(CreateIssueOperation.class);

	/**
	 * permission manager of JIRA.
	 */
	private final PermissionManager permissionManager;

	/**
	 * Atuhentication contxt of JIRA.
	 */
	private final JiraAuthenticationContext jiraAuthenticationContext;

	/**
	 * Descriptor of the jira operation module.
	 */
	private IssueOperationModuleDescriptor descriptor;

	/**
	 * Default constructor.
	 * 
	 * @param aPermissionManager
	 *            : the permission Manager.
	 * @param aJiraAuthenticationContext
	 *            : the context.
	 */
	public CreateIssueOperation(final PermissionManager aPermissionManager,
			final JiraAuthenticationContext aJiraAuthenticationContext) {
		this.permissionManager = aPermissionManager;
		this.jiraAuthenticationContext = aJiraAuthenticationContext;
	}

	/**
	 * Initialization of the descriptor.
	 * 
	 * @param aDescriptor
	 *            : the descriptor of the issue Operation Module.
	 */
	public final void init(final IssueOperationModuleDescriptor aDescriptor) {
		this.descriptor = aDescriptor;
	}

	/**
	 * Generation of the html to displaying the operation.
	 * 
	 * @param issue
	 *            : the current issue.
	 * @return the generated HTML.
	 */
	public final String getHtml(final Issue issue) {
		LinkedIssueContextManager licm = LinkedIssueContextManager.getInstance();
		Mapping[] mappings = null;
		Map params = new HashMap();
		Map mappingIds = new HashMap();
		
		try {
			mappings = licm.getRankedMappingsByIssueParent(issue);
		} catch (LinkNewIssueOperationException e) {
			LOG.error(e.getMessage());
		}
		
		params.put("issueoperation", this);
		params.put("issue", issue);
		params.put("descriptor", descriptor);

		StringBuffer resultHtml = new StringBuffer();
		try{
		// GET THE BEST LINK FROM MAPPING	
		LOG.debug("Unique Value");
		Mapping  mapping = licm.getBestMatchingIssue(mappings);
		
		boolean defaultLinkActivate = licm.isDefaultLinkActivated();
		// TODO
		if(mapping != null){
			String id = String.valueOf(mapping.getId());
			String label = mapping.getLabel();
			params.put("mappingId", id);
			params.put("wording", label);
		}
		if(defaultLinkActivate || (mapping != null)){
			resultHtml.append(getBullet());
			resultHtml.append(descriptor.getHtml("view", params));
			resultHtml.append("</td></tr>");		
		}
		if(licm.isMultiLinkActivated()){
			// GET THE OTHER ASSOCIATED LINKS
			LOG.debug("Multi Value " + licm.isMultiLinkActivated());
			if (mappings != null && mappings.length > 0) {
				LOG.debug("mappings length = " + mappings.length);
				for (int i = 0; i < mappings.length; i++) {
					mapping = mappings[i];
					LOG.debug(" Mapping id : " + mapping.getId() );
					if(mapping.getIsAnAssociatedLink().equalsIgnoreCase(LinkedIssueConstants.TRUE)){
						resultHtml.append("<tr><td class=\"issueOperation lazyOperation\">");
						resultHtml.append(getBullet());
						
						LOG.debug(" Mapping id : " + mapping.getId() +" IS AN ASSOCIATED LINK : " +mapping.getIsAnAssociatedLink());
						String id = String.valueOf(mapping.getId());
						String label = mapping.getLabel();
						params.put("mappingId", id);
						params.put("wording", label);
						resultHtml.append(descriptor.getHtml("view", params));
						resultHtml.append("</td></tr>");
					}else{
						LOG.debug("IS A CONTEXT");
					}
				}
			}
		}
		
		}catch (LinkNewIssueOperationException e) {
			LOG.debug("An error occurs : " + e.getMessage());
		}
		LOG.debug("operations = " + resultHtml.toString());
		return resultHtml.toString();
	}

	/**
	 * This method check user's permission and allows to show or not the
	 * operation link.
	 * 
	 * @param issue
	 *            : the current issue.
	 * @return true if the link should be displayed.
	 */
	public final boolean showOperation(final Issue issue) {
		boolean canCreate;
		boolean canLink;

		User user = jiraAuthenticationContext.getUser();

		try {
			// the actual user should be able to create an issue in a project
			canCreate = permissionManager.hasProjects(Permissions
					.getType("create"), user);
			LOG.debug("The current user can create issue : " + canCreate);
			// and to link in this project
			canLink = permissionManager.hasPermission(Permissions
					.getType("link"), issue.getProjectObject(), user);

			LOG.debug("The current user can Link issue : " + canLink);

			LOG.debug("The Link new issue operation is displayed : "
					+ (canCreate && canLink));
			return (canCreate && canLink);

		} catch (Exception exception) {
			return false;
		}
	}
}