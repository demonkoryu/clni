package com.valiantys.jira.plugins.createissueandlink;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import webwork.action.ActionContext;

import com.atlassian.core.util.FileUtils;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.properties.PropertiesManager;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.customfields.impl.MultiSelectCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.util.AttachmentUtils;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.ImportUtils;
import com.atlassian.jira.web.util.AttachmentException;
import com.atlassian.jira.web.util.OutlookDate;
import com.atlassian.jira.web.util.OutlookDateManager;
import com.atlassian.seraph.auth.DefaultAuthenticator;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.user.EntityNotFoundException;
import com.opensymphony.user.Group;
import com.opensymphony.user.User;
import com.opensymphony.user.UserManager;
import com.valiantys.jira.plugins.createissueandlink.business.LnioContextBean;
import com.valiantys.jira.plugins.createissueandlink.business.LnioFieldsMappingConfiguration;
import com.valiantys.jira.plugins.createissueandlink.business.LnioMappingOfFieldBean;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.IssueChildItem;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.Mapping;
import com.valiantys.jira.plugins.createissueandlink.manager.LnioConfigurationManager;
import com.valiantys.jira.plugins.createissueandlink.util.LinkedIssueConstants;

/**
 * Class implementing methods for the creation and link of new issues through LNIO plugin post function
 * @author Cl�ment Capiaux
 *
 */
public class LnioPFCreateIssueAndLink {

	/**
	 * Logger.
	 */
	protected final static Logger LOG = Logger
			.getLogger(LnioPFCreateIssueAndLink.class);
	private CustomFieldManager cfManager;

	
	
	public LnioPFCreateIssueAndLink() {
		
	}
	
	
	public void createAndLinkNewIssue(LinkedIssueContextManager licm, 
			Issue parentIssue,
			Mapping mapping,
			LnioConfigurationManager lnioConfigManager) throws LinkNewIssueOperationException {
		
		try {
			
			IssueFactory issueFact = ComponentManager.getInstance().getIssueFactory();
			MutableIssue newIssue = issueFact.getIssue();
			
			// post function mapping child fields affectation (except linking)
			newIssue = this.affectMappingFields(newIssue, parentIssue, mapping);
			
			//newIssue = setDefaultValues(newIssue);
			
			// Give the CustomFields a chance to set their default values JRA-11762
            List<CustomField> customFieldObjects = ComponentManager.getInstance().getCustomFieldManager().getCustomFieldObjects(newIssue);
            for (CustomField customField : customFieldObjects)
            {
            	newIssue.setCustomFieldValue(customField, customField.getDefaultValue(newIssue));
            }
			
			// field by field mapping from parent issue
			newIssue = mapFieldsWithInheritValues(lnioConfigManager, newIssue, parentIssue, mapping);
			
			
			
			// save the new issue in the JIRA database
			Map params = new HashMap();
			params.put("issue", newIssue);
			GenericValue issue;
			issue = ComponentManager.getInstance().getIssueManager().createIssue(ComponentManager.getInstance().getJiraAuthenticationContext().getUser(), params);
			issue.store();
			
			// linking the new issue to the parent issue
			this.createLinkBetweenIssues(newIssue, parentIssue, mapping);
			
			// duplication of attachements if it is needed.
			try {
				if (licm.isDuplicateAttachmentsActivated(parentIssue, String.valueOf(mapping.getId())) ) {
					this.duplicatAttachments(parentIssue, newIssue);
				}
			} catch (LinkNewIssueOperationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			// re-indexing the issue
			ImportUtils.setIndexIssues(true);
            ComponentManager.getInstance().getIndexManager().reIndex(newIssue);
            ImportUtils.setIndexIssues(false);
            
			
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GenericEntityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IndexException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
	/**
	 * This function affects the mapping child issue fields to the new issue except the linking
	 * @param newIssue
	 * @param mapping
	 * @return
	 */
	public MutableIssue affectMappingFields(MutableIssue newIssue, Issue parentIssue, Mapping mapping) {
		
		IssueChildItem[] issueChildItems = mapping.getIssueChild().getIssueChildItem();
		
		LOG.debug("Parent issue type id : " + parentIssue.getIssueTypeObject().getId());
		
		// Regular Fields
		for (int i = 0; i < issueChildItems.length; i++) {
			
			if ( issueChildItems[i].getParam().getKey().compareTo(LinkedIssueConstants.ID_TYPE) == 0) {
				
				LOG.debug("Conf issue type value - id  : " + issueChildItems[i].getParam().getValue());
				newIssue.setIssueTypeId(issueChildItems[i].getParam().getValue());
				LOG.debug("Issue type for the new issue updated - id  : " + newIssue.getIssueTypeObject().getId());
			
			}
			
			if ( issueChildItems[i].getParam().getKey().compareTo(LinkedIssueConstants.ID_PROJECT) == 0) {
				
				if ( issueChildItems[i].getParam().getValue().compareTo(LinkedIssueConstants.CURRENT_PRJ) == 0 ) {
					newIssue.setProjectId(parentIssue.getProjectObject().getId());
				}else{
					if (ComponentManager.getInstance().getProjectManager().getProjectObj(Long.valueOf(issueChildItems[i].getParam().getValue())) != null) {
						newIssue.setProjectId(Long.parseLong(issueChildItems[i].getParam().getValue()));
					}
				}
				
			}
			
		}
		
		return newIssue;
		
	}
	
	
	
	
	/**
	 * Creates the link defined in the mapping between the 2 issues (the parent issue and the new issue)
	 * @param newIssue : the new issue created by LNIO
	 * @param parentIssue : the parent issue concerned by the transition
	 * @param mapping : the mapping defined in the configuration file
	 * @throws CreateException
	 */
	public void createLinkBetweenIssues(MutableIssue newIssue, Issue parentIssue, Mapping mapping) {
		
		String mappingLinkDescription = "";
		IssueChildItem[] issueChildItems = mapping.getIssueChild().getIssueChildItem();
		
		IssueLinkManager issueLinkManager = ComponentManager.getInstance().getIssueLinkManager();
		
		
		for (int i = 0; i < issueChildItems.length; i++) {
			
			if (issueChildItems[i].getParam().getKey().compareTo(LinkedIssueConstants.LINK_DESCRIPTION) == 0) {
				mappingLinkDescription = issueChildItems[i].getParam().getValue();
			}
			
		}
		
		Collection<IssueLinkType> linkTypes = ComponentManager.getComponentInstanceOfType(IssueLinkTypeManager.class).getIssueLinkTypes();
		IssueLinkType iltTemp;
		Iterator it = linkTypes.iterator();
		while (it.hasNext()) {
			
			iltTemp = (IssueLinkType) it.next();
			if ( (mappingLinkDescription.compareTo(iltTemp.getInward()) == 0)
					|| (mappingLinkDescription.compareTo(iltTemp.getOutward()) == 0) ){
				
				try {
					if (mappingLinkDescription.compareTo(iltTemp.getInward()) == 0) {
						issueLinkManager.createIssueLink(parentIssue.getId(), newIssue.getId(), iltTemp.getId(), null, (User) ActionContext.getSession().get(DefaultAuthenticator.LOGGED_IN_KEY));
					}else{
						issueLinkManager.createIssueLink(newIssue.getId(), parentIssue.getId(), iltTemp.getId(), null, (User) ActionContext.getSession().get(DefaultAuthenticator.LOGGED_IN_KEY));
					}
				}catch (CreateException e){
					e.printStackTrace();
				}
			}
			
		}
	}
	
	
	/**
	 * Method used to duplicate the attachments form the parent issue to the
	 * child issue.
	 * 
	 * @param issueParent
	 *            : issue parent.
	 * @param issueChild
	 *            : issue child.
	 */
	private void duplicatAttachments(final Issue issueParent,
			final Issue issueChild) {

		AttachmentManager attachmentManager = ComponentManager.getInstance().getAttachmentManager();
		PropertySet propSet = PropertiesManager.getInstance().getPropertySet();
		String attachmentHome = propSet.getString("jira.path.attachments");
		
		ArrayList attachments = (ArrayList) issueParent.getAttachments();
		if (attachments.size() != 0) {
			for (int i = 0; i < attachments.size(); i++) {
				Attachment att = (Attachment) attachments.get(i);
				File origAttachment = AttachmentUtils.getAttachmentFile(att);
				File newAttachment = new File(attachmentHome + File.separator
						+ att.getFilename());
				try {
					FileUtils.copyFile(origAttachment, newAttachment);
					attachmentManager.createAttachment(newAttachment,
							newAttachment.getName(), att.getMimetype(),
							(User) ActionContext.getSession().get(DefaultAuthenticator.LOGGED_IN_KEY), issueChild
									.getGenericValue());
					LOG.debug("The attachments : " + newAttachment.getName()
							+ " is added to the new issue.");
				} catch (IOException e) {
					LOG.error("An error occurs : " + e.getMessage());
				} catch (AttachmentException e) {
					LOG.error("An error occurs : " + e.getMessage());
				} catch (GenericEntityException e) {
					LOG.error("An error occurs : " + e.getMessage());
				}

			}
		}

	}
	
	
	
	
	// TODO refactoring to do
	// MCO : Utiliser la m�thode mapFieldsWithInheritValues du service FieldsMappingService 
	/**
	 * This function realise the field by field mapping between the parent issue and the new child issue created
	 * @param lnioConfigManager : the field by field mapping configuration object
	 * @param newIssue : the new issue created
	 * @param parentIssue : the parent issue
	 * @param mapping : the mapping defined for the LNIO post function
	 * @return
	 */
	private MutableIssue mapFieldsWithInheritValues(LnioConfigurationManager lnioConfigManager, 
			MutableIssue newIssue, 
			Issue parentIssue, 
			Mapping mapping) {
		
		LnioMappingOfFieldBean fieldMappingTemp;
		String sourceFieldId = "";
		String destinationFieldId = "";
		FieldManager fieldManager = ComponentManager.getInstance().getFieldManager();
		CustomFieldManager cfManager = ComponentManager.getInstance().getCustomFieldManager();
		boolean isReporterMapped = false;
		boolean isSummaryMapped = false;
		
		LnioFieldsMappingConfiguration lnioFieldsConfig = null;
		try {
			lnioFieldsConfig = lnioConfigManager.getLnioFieldsMappingConfigurationObject();
		} catch (LinkNewIssueOperationException e) {
			e.printStackTrace();
			LOG.error("Failed to load LNIO configuration");
		}
		
		LnioContextBean lcb = (LnioContextBean) lnioFieldsConfig.getConfiguredContexts().get(String.valueOf(mapping.getId()));
		List fieldsMapping = lcb.getListOfFieldsMapping();
		
		if (fieldsMapping != null) { 
		
			Iterator it = fieldsMapping.iterator();
		
			while (it.hasNext()) {
				
				fieldMappingTemp = (LnioMappingOfFieldBean) it.next();
				
				sourceFieldId = fieldMappingTemp.getOriginFieldId();
				destinationFieldId = fieldMappingTemp.getDestinationFieldId();
				
				Object newValue = null;
				
				// if destination field is a custom field (its id is "customfield_xxx")
				if ( destinationFieldId.startsWith("customfield_") ) {
					
					// if source field is a custom field (its id is "customfield_xxx")
					if ( sourceFieldId.startsWith("customfield_") ) {
						
						if (sourceFieldId.compareTo(destinationFieldId) != 0) {
							
							LOG.debug("sourceFieldId : " + sourceFieldId);
							LOG.debug("destinationFieldId : " + destinationFieldId);
							
							OutlookDateManager outlookDateManager = ComponentManager.getInstance().getOutlookDateManager();
							JiraAuthenticationContext authenticationContext = ComponentManager.getInstance().getJiraAuthenticationContext();
							
							Object cfValue = parentIssue.getCustomFieldValue((CustomField) fieldManager.getCustomField(sourceFieldId));
							if (cfValue instanceof User) {
								newValue = ((User) cfValue).getName();
							} else if (cfValue instanceof Group) {
								newValue = ((Group) cfValue).getName();
							} else if (cfValue instanceof Project) {
								newValue = ((Project) cfValue).getName();
							} else if (cfValue instanceof ProjectRole) {
								newValue = ((ProjectRole) cfValue).getName();
							} else if (cfValue instanceof String) {
								newValue = cfValue.toString();
							} else if (cfValue instanceof Date) {
								OutlookDate outlookDate = outlookDateManager
										.getOutlookDate(authenticationContext.getLocale());
								newValue = outlookDate.format((Date) cfValue);
							} else if (cfValue instanceof Timestamp) {
								OutlookDate outlookDate = outlookDateManager
										.getOutlookDate(authenticationContext.getLocale());
								newValue = outlookDate.format(new Date(((Timestamp) cfValue)
										.getTime()));
							} else if (cfValue instanceof Double) {
								newValue = new Float( ((Double) cfValue).floatValue() ).toString();
								
							} else {
								newValue = cfValue;
							}
							
							
							
							List customFieldParams = new ArrayList();
							
							if (newValue instanceof ArrayList) {
								
								Object objectTmp = null;
								
								ArrayList l = (ArrayList) newValue;
								for (int i = 0; i < l.size(); i++) {
									if (l.get(i) instanceof User) {
										objectTmp = ((User) cfValue).getName();
									} else if (l.get(i) instanceof Group) {
										objectTmp = ((Group) cfValue).getName();
									} else if (l.get(i) instanceof Project) {
										objectTmp = ((Project) cfValue).getName();
									} else if (l.get(i) instanceof ProjectRole) {
										objectTmp = ((ProjectRole) cfValue).getName();
									} else if (l.get(i) instanceof String) {
										objectTmp = cfValue.toString();
									} else if (l.get(i) instanceof Date) {
										OutlookDate outlookDate = outlookDateManager
												.getOutlookDate(authenticationContext.getLocale());
										objectTmp = outlookDate.format((Date) l.get(i));
									} else if (l.get(i) instanceof Timestamp) {
										OutlookDate outlookDate = outlookDateManager
												.getOutlookDate(authenticationContext.getLocale());
										objectTmp = outlookDate.format(new Date(((Timestamp) l.get(i))
												.getTime()));
									} else if (l.get(i) instanceof Double) {
										objectTmp = new Float( ((Double) l.get(i)).floatValue() ).toString();
										
									} else {
										objectTmp = l.get(i);
									}
									customFieldParams.add(objectTmp);
								}
								
								newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), customFieldParams);
								
							}else{
								
								// if destination field is a multi select list
								if ( cfManager.getCustomFieldObject(destinationFieldId).getCustomFieldType() instanceof MultiSelectCFType ) {
									List multiSelectDestinationValue = new ArrayList();
									multiSelectDestinationValue.add(parentIssue.getCustomFieldValue(cfManager.getCustomFieldObject(sourceFieldId)).toString());
									newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), multiSelectDestinationValue);
								}else{
									newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getCustomFieldValue(cfManager.getCustomFieldObject(sourceFieldId)));
								}
								
							}
							
						}else{
							
							newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getCustomFieldValue(cfManager.getCustomFieldObject(sourceFieldId)));
						}
						
							
					}else{
						
						if (sourceFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {
							newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getEnvironment());
						}
						if (sourceFieldId.compareTo(IssueFieldConstants.STATUS) == 0) {
							newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getStatusObject().getName());
						}
						if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
							newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getResolutionDate());
						}
						if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
							newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getDueDate());
						}
						if (sourceFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
							newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getFixVersions());
						}
						if (sourceFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
							newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getDescription());
						}
						if (sourceFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0) {
							newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getPriority());
						}
						if (sourceFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
							newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getComponents());
						}
						if (sourceFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
							newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getSecurityLevel());
						}
						if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
							newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getResolution());
						}
						if (sourceFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
							newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getAffectedVersions());
						}
						if (sourceFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
							newIssue.setCustomFieldValue(cfManager.getCustomFieldObject(destinationFieldId), parentIssue.getSummary());
						}
						
					}
					
				}else{
					
					// if source field is a custom field (its id is "customfield_xxx")
					if ( sourceFieldId.startsWith("customfield_") ) {
						
						Object sourceCustomFieldValue = parentIssue.getCustomFieldValue(cfManager.getCustomFieldObject(sourceFieldId));
						
						if (destinationFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {
							newIssue.setEnvironment((String) sourceCustomFieldValue);
						}
						if (destinationFieldId.compareTo(IssueFieldConstants.STATUS) == 0) {
							newIssue.setStatus((GenericValue) sourceCustomFieldValue);
						}
						if (destinationFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
							newIssue.setResolutionDate((Timestamp) sourceCustomFieldValue);
						}
						if (destinationFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
							newIssue.setDueDate((Timestamp) sourceCustomFieldValue);
						}
						if (destinationFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
							newIssue.setFixVersions((Collection<Version>) sourceCustomFieldValue);
						}
						if (destinationFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
							newIssue.setDescription((String) sourceCustomFieldValue);
						}
						if (destinationFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0) {
							newIssue.setPriority((GenericValue) sourceCustomFieldValue);
						}
						if (destinationFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
							newIssue.setComponents((Collection<GenericValue>) sourceCustomFieldValue);
						}
						if (destinationFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
							newIssue.setSecurityLevel((GenericValue) sourceCustomFieldValue);
						}
						if (destinationFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
							newIssue.setResolution((GenericValue) sourceCustomFieldValue);
						}
						if (destinationFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
							newIssue.setAffectedVersions((Collection<Version>) sourceCustomFieldValue);
						}
						if (destinationFieldId.compareTo(IssueFieldConstants.REPORTER) == 0) {
							newIssue.setReporter((User) sourceCustomFieldValue);
						}
						if (destinationFieldId.compareTo(IssueFieldConstants.ASSIGNEE) == 0) {
							newIssue.setAssignee((User) sourceCustomFieldValue);
						}
						if (destinationFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
							newIssue.setSummary(sourceCustomFieldValue.toString());
							isSummaryMapped = true;
						}
						
					}else{
						
						// REPORTER
						if (destinationFieldId.compareTo(IssueFieldConstants.REPORTER) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.REPORTER) == 0) {
								newIssue.setReporter(parentIssue.getReporter());
								isReporterMapped = true;
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.ASSIGNEE) == 0) {
								newIssue.setReporter(parentIssue.getAssignee());
								isReporterMapped = true;
							}
						}
						
						// ASSIGNEE
						if (destinationFieldId.compareTo(IssueFieldConstants.ASSIGNEE) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.REPORTER) == 0) {
								newIssue.setAssignee(parentIssue.getReporter());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.ASSIGNEE) == 0) {
								newIssue.setAssignee(parentIssue.getAssignee());
							}
						}
						
						// SUMMARY
						if (destinationFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {
								newIssue.setSummary(parentIssue.getEnvironment());
								isSummaryMapped = true;
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
								newIssue.setSummary(parentIssue.getResolutionDate().toString());
								isSummaryMapped = true;
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
								newIssue.setSummary(parentIssue.getDueDate().toString());
								isSummaryMapped = true;
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
								newIssue.setSummary(parentIssue.getDescription());
								isSummaryMapped = true;
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
								newIssue.setSummary(parentIssue.getSummary());
								isSummaryMapped = true;
							}
						}
						
						// ENVIRONMENT
						if (destinationFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {
								newIssue.setEnvironment(parentIssue.getEnvironment());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.STATUS) == 0) {
								newIssue.setEnvironment(parentIssue.getStatusObject().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
								newIssue.setEnvironment(parentIssue.getResolutionDate().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
								newIssue.setEnvironment(parentIssue.getDueDate().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
								newIssue.setEnvironment(parentIssue.getFixVersions().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
								newIssue.setEnvironment(parentIssue.getDescription());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0) {
								newIssue.setEnvironment(parentIssue.getPriority().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
								newIssue.setEnvironment(parentIssue.getComponents().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
								newIssue.setEnvironment(parentIssue.getSecurityLevel().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
								newIssue.setEnvironment(parentIssue.getResolution().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
								newIssue.setEnvironment(parentIssue.getAffectedVersions().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
								newIssue.setEnvironment(parentIssue.getSummary());
							}
						}
						
						// STATUS
						if (destinationFieldId.compareTo(IssueFieldConstants.STATUS) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {
								newIssue.setStatusId(parentIssue.getEnvironment());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.STATUS) == 0) {
								newIssue.setStatusId(parentIssue.getStatusObject().getId());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
								newIssue.setStatusId(parentIssue.getResolutionDate().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
								newIssue.setStatusId(parentIssue.getDueDate().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
								newIssue.setStatusId(parentIssue.getFixVersions().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
								newIssue.setStatusId(parentIssue.getDescription());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0) {
								newIssue.setStatusId(parentIssue.getPriorityObject().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
								newIssue.setStatusId(parentIssue.getComponents().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
								newIssue.setStatusId(parentIssue.getSecurityLevel().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
								newIssue.setStatusId(parentIssue.getResolution().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
								newIssue.setStatusId(parentIssue.getAffectedVersions().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
								newIssue.setStatusId(parentIssue.getSummary());
							}
						}
						
						// RESOLUTION DATE
						if (destinationFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
								newIssue.setResolutionDate(parentIssue.getResolutionDate());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
								newIssue.setResolutionDate(parentIssue.getDueDate());
							}
						}
						
						// DUE DATE
						if (destinationFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
								newIssue.setDueDate(parentIssue.getResolutionDate());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
								newIssue.setDueDate(parentIssue.getDueDate());
							}
						}
						
						// FIX VERSIONS
						if (destinationFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
								newIssue.setFixVersions(parentIssue.getFixVersions());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
								newIssue.setFixVersions(parentIssue.getAffectedVersions());
							}
						}
						
						// DESCRIPTION
						if (destinationFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.ENVIRONMENT) == 0) {
								newIssue.setDescription(parentIssue.getEnvironment());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.STATUS) == 0) {
								newIssue.setDescription(parentIssue.getStatusObject().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION_DATE) == 0) {
								newIssue.setDescription(parentIssue.getResolutionDate().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.DUE_DATE) == 0) {
								newIssue.setDescription(parentIssue.getDueDate().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
								newIssue.setDescription(parentIssue.getFixVersions().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.DESCRIPTION) == 0) {
								newIssue.setDescription(parentIssue.getDescription());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0) {
								newIssue.setDescription(parentIssue.getPriorityObject().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
								newIssue.setDescription(parentIssue.getComponents().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
								newIssue.setDescription(parentIssue.getSecurityLevel().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
								newIssue.setDescription(parentIssue.getResolution().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
								newIssue.setDescription(parentIssue.getAffectedVersions().toString());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.SUMMARY) == 0) {
								newIssue.setDescription(parentIssue.getSummary());
							}
						}
						
						// PRIORITY
						if (destinationFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.PRIORITY) == 0) {
								newIssue.setPriority(parentIssue.getPriority());
							}
						}
						
						// COMPONENTS
						if (destinationFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.COMPONENTS) == 0) {
								newIssue.setComponents(parentIssue.getComponents());
							}
						}
						
						// SECURITY
						if (destinationFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.SECURITY) == 0) {
								newIssue.setSecurityLevel(parentIssue.getSecurityLevel());
							}
						}
						
						// RESOLUTION
						if (destinationFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.RESOLUTION) == 0) {
								newIssue.setResolution(parentIssue.getResolution());
							}
						}
						
						// AFFECTED VERSIONS
						if (destinationFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
							if (sourceFieldId.compareTo(IssueFieldConstants.FIX_FOR_VERSIONS) == 0) {
								newIssue.setAffectedVersions(parentIssue.getFixVersions());
							}
							if (sourceFieldId.compareTo(IssueFieldConstants.AFFECTED_VERSIONS) == 0) {
								newIssue.setAffectedVersions(parentIssue.getAffectedVersions());
							}
						}
						
					}
					
				
				
				}
			
			}
		
		}
		
		if (!isReporterMapped) {
			newIssue.setReporter(parentIssue.getReporter());
		}
		
		if (!isSummaryMapped) {
			I18nHelper i18n = ComponentManager.getInstance().getJiraAuthenticationContext().getI18nHelper();
			newIssue.setSummary(i18n.getText("summary.prefix") + " : " + parentIssue.getSummary());
		}
		
		return newIssue;
	
	}
	
}
