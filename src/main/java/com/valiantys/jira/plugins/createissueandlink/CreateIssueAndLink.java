package com.valiantys.jira.plugins.createissueandlink;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.customfields.impl.DateCFType;
import com.atlassian.jira.issue.customfields.impl.DateTimeCFType;
import com.atlassian.jira.issue.customfields.view.CustomFieldParamsImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.IssueTypeSystemField;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.ProjectSystemField;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderTab;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.web.action.issue.CreateIssue;
import com.atlassian.jira.web.action.issue.IssueCreationHelperBean;
import com.atlassian.jira.web.util.OutlookDate;
import com.atlassian.jira.web.util.OutlookDateManager;
import com.opensymphony.user.EntityNotFoundException;
import com.opensymphony.user.Group;
import com.opensymphony.user.User;
import com.valiantys.jira.plugins.createissueandlink.business.LnioContextBean;
import com.valiantys.jira.plugins.createissueandlink.business.LnioFieldsMappingConfiguration;
import com.valiantys.jira.plugins.createissueandlink.business.LnioMappingOfFieldBean;
import com.valiantys.jira.plugins.createissueandlink.lnioCastor.SkippedFieldsItem;
import com.valiantys.jira.plugins.createissueandlink.manager.ILnioConfigurationManager;
import com.valiantys.jira.plugins.createissueandlink.util.LinkedIssueConstants;

/**
 * webaction used to create an new issue automatically linked with the current
 * issue. Differents context are defined in a xml file. The fields inheritance
 * is implemented.
 * 
 * @author Mathieu AGAR (mathieu.agar@valiantys.com)
 * @since 1.0
 * 
 */
public class CreateIssueAndLink extends CreateIssue {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 7831313391165364644L;

	/**
	 * Logger.
	 */
	private static final Logger LOG = Logger.getLogger(CreateIssueAndLink.class);

	/**
	 * return error page.
	 */
	private static final String PLUGIN_ERROR = "pluginerror";

	/**
	 * return subtask creation page
	 */
	private static final String SUBTASK = "subtask";

	/**
	 * Factory which create issue.
	 */
	private final IssueFactory issueFactory;

	/**
	 * Issue Manager.
	 */
	private final IssueManager issueManager;

	/**
	 * Link manager.
	 */
	private final IssueLinkTypeManager issueLinkTypeManager;

	/**
	 * Project Manager.
	 */
	private final ProjectManager projectManager;

	/**
	 * Constants manager of JIRA.
	 */
	private final ConstantsManager constantsManager;

	/**
	 * User-selected link operation (e.g. "duplicates").
	 */
	private String linkDesc;

	/**
	 * List of available links.
	 */
	private List linkDescs;

	/**
	 * The original issue that is to be linked to.
	 */
	private Issue originalIssue;

	/**
	 * The type of the selected link (e.g. LINK-1).
	 */
	private String linkTypeName;

	/**
	 * Id of the parent linked issue (current issue for the operation).
	 */
	private String parentid;
	private Long parentIssueId;

	/**
	 * Id of the mapping (also called context) defined in the xml file.
	 */
	private String mappingid;

	/**
	 * error message to display.
	 */
	private String errorMessage;

	/**
	 * The LNIO configuration object from db.
	 */
	private LnioFieldsMappingConfiguration lnioConfig = null;

	/**
	 * Configuration Manager of LNIO.
	 */
	private final ILnioConfigurationManager lnioConfigManager;

	/**
	 * Configuration Manager of LNIO.
	 */
	private final OutlookDateManager outlookDateManager;

	/**
	 * Authentication context.
	 */
	private final JiraAuthenticationContext authenticationContext;

	/**
	 * Current LNIO context Bean, used by the specific.
	 */
	private LnioContextBean contextBean;

	private IssueInputParameters issueInputParameters;

	/**
	 * The constructor of the action.
	 * 
	 * @param issueCreationHelperBean
	 *            : issueCreationHelperBean.
	 * @param issueFactory
	 *            : issueFactory.
	 * @param issueManager
	 *            : issueManager.
	 * @param issueLinkTypeManager
	 *            : issueLinkTypeManager.
	 * @param projectManager
	 *            : projectManager.
	 * @param constantsManager
	 *            : constantsManager.
	 */
	public CreateIssueAndLink(
			final IssueCreationHelperBean issueCreationHelperBean,
			final IssueFactory issueFactory, final IssueManager issueManager,
			final IssueLinkTypeManager issueLinkTypeManager,
			final ProjectManager projectManager,
			final ConstantsManager constantsManager,
			final ILnioConfigurationManager lnioConfManager,
			final JiraAuthenticationContext context,
			final OutlookDateManager outlookDateManager
	) {
		super(issueFactory, issueCreationHelperBean);
		this.issueFactory = issueFactory;
		this.issueManager = issueManager;
		this.issueLinkTypeManager = issueLinkTypeManager;
		this.projectManager = projectManager;
		this.constantsManager = constantsManager;
		this.lnioConfigManager = lnioConfManager;
		this.outlookDateManager = outlookDateManager;
		this.authenticationContext = context;
	}

	/**
	 * {@inheritDoc}
	 */
	public final String doDefault() throws Exception {
		LOG.debug("Parentid is currently:" + parentid
				+ " - Mappingid is currently : " + mappingid);

		LinkedIssueContextManager LNIOMng = LinkedIssueContextManager
		.getInstance();
		// set the id of the issue.
		setId(Long.valueOf(parentid));

		// Check if the Link type is defined.
		String link = LNIOMng.getLinkDescription(getIssueById(Long
				.valueOf(parentid)), mappingid);
		boolean isLinkTypeWellDefined = (LNIOMng.getLinkDescription(
				getIssueById(Long.valueOf(parentid)), mappingid) != null && getLinkDescs()
				.contains(link));
		LOG.debug("isLinkTypeWellDefined : " + isLinkTypeWellDefined);

		// Check if the Issue type is defined.
		String issueTypeId = LNIOMng.getIssueTypeIdDoDefault(getIssueById(Long
				.valueOf(parentid)), mappingid);
		boolean isIssueTypeWellDefined = (issueTypeId != null && constantsManager
				.getAllIssueTypeIds().contains(issueTypeId));
		LOG.debug("isIssueTypeWellDefined : " + isIssueTypeWellDefined);

		//Check if the JIRA_SUBTASK defined
		String jiraSubTaskSpecified = LNIOMng.getJIRASubtaskValue(getIssueById(Long
				.valueOf(parentid)), mappingid);
		LOG.debug("JIRA sub task value : " + jiraSubTaskSpecified);


		// Check if the project child is defined.
		LOG.debug("isProjectChildDefined : " + isProjectChildDefined(parentid));
		try {
			if (isLinkedIssueAssociatedLink(parentid)) {
				if (isLinkTypeWellDefined && isIssueTypeWellDefined	&& isProjectChildDefined(parentid) && jiraSubTaskSpecified == null) {
					return generateLinkedIssueContextualized(issueTypeId);
				} else {

					if ( (jiraSubTaskSpecified != null) && (jiraSubTaskSpecified.equals("true")) && isIssueTypeWellDefined ) {
						return generateSubTaskIssueContextualized(issueTypeId);
					}

					if (isLinkTypeWellDefined) {
						setLinkDesc(link);
						if (isIssueTypeWellDefined) {
							LOG.debug("issueType (" + issueTypeId
									+ ") well defined");
							setIssuetype(issueTypeId);
							LOG.debug("project not defined");
						} else {
							LOG.warn("issue type (" + issueTypeId
									+ ") not defined, go to choice view");
							if (isProjectChildDefined(parentid)) {
								String childIssueProjectId = LNIOMng.getProjectDefinedForChildIssueInMappingOfParentIssue(getIssueById(Long
										.valueOf(parentid)),mappingid);
								if (childIssueProjectId.equals(LinkedIssueConstants.CURRENT_PRJ)) {
									LOG.debug("project defined is CURRENT PROJECT ("+ getIssueById(Long.valueOf(parentid))
											.getProjectObject()
											.getId());

									setPid(getIssueById(
											Long.valueOf(parentid))
											.getProjectObject().getId());
								} else {
									LOG
									.debug("project defined is"
											+ Long
											.valueOf(childIssueProjectId));
									setPid(Long
											.valueOf(childIssueProjectId));
								}
								getFieldValuesHolder().put("project", getPid());

							} else {
								LOG.debug("project not defined");
							}

						}
					} else {
						LOG.warn("Link type : " + link
								+ " not defined, go to choice view");
						if (isIssueTypeWellDefined) {
							setIssuetype(issueTypeId);
							if (isProjectChildDefined(parentid)) {
								String childIssueProjectId = LNIOMng
								.getProjectDefinedForChildIssueInMappingOfParentIssue(
										getIssueById(Long
												.valueOf(parentid)),
												mappingid);
								if (childIssueProjectId
										.equals(LinkedIssueConstants.CURRENT_PRJ)) {
									LOG
									.debug("project defined is CURRENT PROJECT ("
											+ getIssueById(
													Long
													.valueOf(parentid))
													.getProjectObject()
													.getId());
									setSelectedProjectId(getIssueById(
											Long.valueOf(parentid))
											.getProjectObject().getId());
								} else {
									LOG
									.debug("project defined is "
											+ Long
											.valueOf(childIssueProjectId));
									setSelectedProjectId(Long
											.valueOf(childIssueProjectId));
								}
							}
						} else {
							LOG.warn("issue type id : " + issueTypeId
									+ " not defined, back to choice view");
							if (isProjectChildDefined(parentid)) {
								String childIssueProjectId = LNIOMng
								.getProjectDefinedForChildIssueInMappingOfParentIssue(
										getIssueById(Long
												.valueOf(parentid)),
												mappingid);
								if (childIssueProjectId
										.equals(LinkedIssueConstants.CURRENT_PRJ)) {
									LOG
									.debug("project defined is CURRENT PROJECT ("
											+ getIssueById(
													Long
													.valueOf(parentid))
													.getProjectObject()
													.getId());
									super.setPid(getIssueById(
											Long.valueOf(parentid))
											.getProjectObject().getId());
								} else {
									LOG
									.debug("project defined is "
											+ Long
											.valueOf(childIssueProjectId));
									super.setPid(Long
											.valueOf(childIssueProjectId));
								}
							}
						}
					}
					return super.doDefault();
				}
			} else {
				return super.doDefault();
			}
		} catch (LinkNewIssueOperationException e) {
			setErrorMessage(e.getException().getMessage());
			return PLUGIN_ERROR;
		}
	}

	/**
	 * initialization of the creation of the linked issue.
	 * 
	 * @return the result screen. succes or error.
	 * @throws Exception
	 *             :Exception.
	 */
	protected final String doExecute() throws Exception {
		LinkedIssueContextManager LNIOMng = LinkedIssueContextManager.getInstance();

		LOG.debug("CreateIssueAndLink: parentid is currently:" + parentid);
		// Parent issue
		Issue parentIssue = getIssueById(Long.valueOf(parentid));
		if (parentIssue == null) {
			throw new Exception(
					"CreateIssueAndLink: Unable to find issue with id: "
					+ parentid);
		}
		setOriginalIssue(parentIssue);

		// Link type
		LOG.debug("CreateIssueAndLink: user-defined link type is " + linkDesc
				+ ", now looking for it in existing linktypes");
		this.setLinkTypeName(linkDesc);

		// return super.doExecute();
		ProjectSystemField projectField = (ProjectSystemField) getField("project");
		projectField.updateIssue(null, getIssueObject(),
				getFieldValuesHolder());
		IssueTypeSystemField issueTypeField = (IssueTypeSystemField) getField("issuetype");
		issueTypeField.updateIssue(null, getIssueObject(),
				getFieldValuesHolder());
		recordHistoryIssueType();
		setSelectedProjectId(getPid());

		try {
			if (lnioConfig == null) {
				lnioConfig = loadLNIOConf();
				LOG.debug("getIssueWithSpecificFieldsMapping : " + lnioConfig);
			}

			if (isLinkedIssueAssociatedLink(parentid)) {
				boolean inheritance = LNIOMng.isLinkedIssueInheritanceActivated() && LNIOMng.isInheritanceActivatedForThisMapping(mappingid);

				if (inheritance
						|| (LNIOMng.isSpecificMappingActivated(lnioConfig,
								mappingid))) {
					// Case inheritance actif
					populateFieldHolderWithInheritValues(parentIssue,
							inheritance);
				} else {
					// case no inheritance no specific mapping
					populateFieldHolderWithDefaults(getIssueObject(),
							Collections.EMPTY_LIST);
				}
			} else {
				boolean inheritance = LNIOMng.isLinkedIssueInheritanceActivated();
				if (inheritance) {
					// Case inheritance actif
					populateFieldHolderWithInheritValues(parentIssue,
							inheritance);
				} else {
					populateFieldHolderWithDefaults(getIssueObject(),
							Collections.EMPTY_LIST);
				}
			}

		} catch (LinkNewIssueOperationException e) {
			setErrorMessage(e.getException().getMessage());

			return PLUGIN_ERROR;
		}
		return "success";
	}

	/**
	 * Initialize the creation of the linked issue based on the context.
	 * 
	 * @return the result screen. succes or error.
	 * @throws Exception
	 *             : Exception.
	 */
	private String generateLinkedIssueContextualized(String issueTypeIdParam) throws Exception {
		super.doDefault();

		// retrieve the parent issue
		Issue parentIssue = getIssueById(Long.valueOf(parentid));
		if (parentIssue == null) {
			throw new Exception(
					"CreateIssueAndLink: Unable to find issue with id: "
					+ parentid);
		}

		// retrieve the link description form mapping.
		linkDesc = LinkedIssueContextManager.getInstance().getLinkDescription(
				parentIssue, mappingid);
		this.setLinkTypeName(linkDesc);

		// retrieve the project from mapping.
		String childIssueProjectId = LinkedIssueContextManager.getInstance()
		.getProjectDefinedForChildIssueInMappingOfParentIssue(
				parentIssue, mappingid);
		String pid = null;
		if (childIssueProjectId.equals(LinkedIssueConstants.CURRENT_PRJ)) {
			LOG.debug("Project Id for Child Issue Is "
					+ LinkedIssueConstants.CURRENT_PRJ);
			pid = String.valueOf(parentIssue.getProjectObject().getId());
		} else {
			if (projectManager.getProjectObj(Long.valueOf(childIssueProjectId)) != null) {
				pid = childIssueProjectId;
			} else {
				LOG
				.warn("Project Id configured for Child Issue Is not valid, back to choice project view");
				linkDesc = LinkedIssueContextManager.getInstance()
				.getLinkDescription(parentIssue, mappingid);
				setLinkTypeName(linkDesc);
				String issueTypeId = LinkedIssueContextManager.getInstance()
				.getIssueTypeId(parentIssue, mappingid);
				setIssuetype(issueTypeId);
				super.setPid(null);
				return super.doDefault();
			}
		}
		super.setPid(Long.valueOf(pid));
		getFieldValuesHolder().put("project", Long.valueOf(pid));
		ProjectSystemField projectField = (ProjectSystemField) getField(IssueFieldConstants.PROJECT);
		projectField.updateIssue(null, getIssueObject(),
				getFieldValuesHolder());

		// retrieve the issue type mapping.
		String issueTypeId;
		if(issueTypeIdParam == null) {		
			issueTypeId = LinkedIssueContextManager.getInstance()
			.getIssueTypeId(parentIssue, mappingid);
		}else{
			issueTypeId = issueTypeIdParam;	
		}
		setIssuetype(issueTypeId);
		getFieldValuesHolder().put("issuetype", issueTypeId);
		IssueTypeSystemField issueTypeField = (IssueTypeSystemField) getField(IssueFieldConstants.ISSUE_TYPE);
		issueTypeField.updateIssue(null, getIssueObject(),
				getFieldValuesHolder());

		LOG.debug("Link Description setted for this new issue : " + linkDesc);
		LOG.debug("Project Id setted for this new issue : " + pid);
		LOG.debug("Issue Type Id setted for this new issue : " + issueTypeId);

		if (issueTypeId == null || pid == null || linkDesc == null) {
			throw new Exception(
			"The mapping of contextualized issue contains error. The issue type, project and link description can not be null");
		}

		// record history
		recordHistoryIssueType();
		setSelectedProjectId(Long.valueOf(pid));

		// populating the field for the next screen depending of the issue type
		// scheme and screen.
		try {

			if (lnioConfig == null) {
				lnioConfig = loadLNIOConf();
				LOG.debug("getIssueWithSpecificFieldsMapping : " + lnioConfig);
			}
			LinkedIssueContextManager LNIOMng = LinkedIssueContextManager.getInstance();

			if (isLinkedIssueAssociatedLink(parentid)) {
				boolean inheritance = LNIOMng
				.isLinkedIssueInheritanceActivated()
				&& LNIOMng
				.isInheritanceActivatedForThisMapping(mappingid);
				if (inheritance
						|| (LNIOMng.isSpecificMappingActivated(lnioConfig,
								mappingid))) {
					// Case inheritance actif
					populateFieldHolderWithInheritValues(parentIssue,
							inheritance);
				} else {
					// case no inheritance no specific mapping
					populateFieldHolderWithDefaults(getIssueObject(),
							Collections.EMPTY_LIST);
				}
			} else {
				boolean inheritance = LNIOMng.isLinkedIssueInheritanceActivated();
				if (inheritance) {
					// Case inheritance actif
					populateFieldHolderWithInheritValues(parentIssue,
							inheritance);
				} else {
					populateFieldHolderWithDefaults(getIssueObject(),
							Collections.EMPTY_LIST);
				}
			}
		} catch (LinkNewIssueOperationException e) {
			setErrorMessage(e.getException().getMessage());
			return PLUGIN_ERROR;
		}
		return SUCCESS;
	}



	/**
	 * Initialize the creation of the linked issue based on the context.
	 * 
	 * @return the result screen. succes or error.
	 * @throws Exception
	 *             : Exception.
	 */
	private String generateSubTaskIssueContextualized(String issueTypeIdParam) throws Exception {

		LOG.debug("ON VA GENERER LA SUB TASK CONTEXTUALIZED");

		super.doDefault();

		// retrieve the parent issue
		Issue parentIssue = getIssueById(Long.valueOf(parentid));
		if (parentIssue == null) {
			throw new Exception(
					"CreateIssueAndLink: Unable to find issue with id: "
					+ parentid);
		}

		// retrieve the link description form mapping.
		/*
		linkDesc = LinkedIssueContextManager.getInstance().getLinkDescription(
				parentIssue, mappingid);
		this.setLinkTypeName(linkDesc);
		 */

		// retrieve the project from mapping.
		String pid = null;
		pid = String.valueOf(parentIssue.getProjectObject().getId());
		super.setPid(Long.valueOf(pid));
		getFieldValuesHolder().put("project", Long.valueOf(pid));
		ProjectSystemField projectField = (ProjectSystemField) getField(IssueFieldConstants.PROJECT);
		projectField.updateIssue(null, getIssueObject(),
				getFieldValuesHolder());
		// retrieve the issue type mapping.
		
		String issueTypeId;
		
		if(issueTypeIdParam == null) {
			issueTypeId = LinkedIssueContextManager.getInstance().getIssueTypeId(parentIssue, mappingid);
		}else{
			issueTypeId = issueTypeIdParam;
		}
		setIssuetype(issueTypeId);
		getFieldValuesHolder().put("issuetype", issueTypeId);
		IssueTypeSystemField issueTypeField = (IssueTypeSystemField) getField(IssueFieldConstants.ISSUE_TYPE);
		issueTypeField.updateIssue(null, getIssueObject(),
				getFieldValuesHolder());

		LOG.debug("Project Id setted for this new issue : " + pid);
		LOG.debug("Issue Type Id setted for this new issue : " + issueTypeId);

		LOG.debug("PARENT ID : " + getParentid());

		setParentIssueId(parentid);

		/*
		if (issueTypeId == null || pid == null || linkDesc == null) {
			throw new Exception(
					"The mapping of contextualized issue contains error. The issue type, project and link description can not be null");
		}
		 */

		// record history
		recordHistoryIssueType();
		setSelectedProjectId(Long.valueOf(pid));

		// populating the field for the next screen depending of the issue type
		// scheme and screen.
		try {

			if (lnioConfig == null) {
				lnioConfig = loadLNIOConf();
				LOG.debug("getIssueWithSpecificFieldsMapping : " + lnioConfig);
			}
			LinkedIssueContextManager LNIOMng = LinkedIssueContextManager.getInstance();

			if (isLinkedIssueAssociatedLink(parentid)) {
				boolean inheritance = LNIOMng
				.isLinkedIssueInheritanceActivated()
				&& LNIOMng
				.isInheritanceActivatedForThisMapping(mappingid);
				if (inheritance
						|| (LNIOMng.isSpecificMappingActivated(lnioConfig,
								mappingid))) {
					// Case inheritance actif
					populateFieldHolderWithInheritValues(parentIssue,
							inheritance);
				} else {
					// case no inheritance no specific mapping
					populateFieldHolderWithDefaults(getIssueObject(),
							Collections.EMPTY_LIST);
				}
			} else {
				boolean inheritance = LNIOMng.isLinkedIssueInheritanceActivated();
				if (inheritance) {
					// Case inheritance actif
					populateFieldHolderWithInheritValues(parentIssue,
							inheritance);
				} else {
					populateFieldHolderWithDefaults(getIssueObject(),
							Collections.EMPTY_LIST);
				}
			}
		} catch (LinkNewIssueOperationException e) {
			setErrorMessage(e.getException().getMessage());
			return PLUGIN_ERROR;
		}
		LOG.debug("ON RETOURNE BIEN 'SUBTASK'");
		return SUBTASK;
	}



	/**
	 * This method checked if the creation of the new issue is based on an
	 * associated link.
	 * 
	 * @param parentIssueId
	 *            : od of the parent issue.
	 * @return true if we have to take in account the mapping of an associated
	 *         link.
	 * @throws LinkNewIssueOperationException
	 *             : a LinkNewIssueOperationException.
	 */
	private boolean isLinkedIssueAssociatedLink(final String parentIssueId)
	throws LinkNewIssueOperationException {
		boolean isConfActivated = LinkedIssueContextManager.getInstance()
		.isMultiLinkActivated();
		boolean isParentIssueMapped = LinkedIssueContextManager.getInstance()
		.isIssueMapped(getIssueById(Long.valueOf(parentIssueId)),
				mappingid);
		LOG.debug("Linked issue 'Active' configured : " + isConfActivated);
		LOG.debug("Is parent issue 'mapped' : " + isParentIssueMapped);
		return (isConfActivated && isParentIssueMapped);
	}

	/**
	 * This method checked if the new issue has a project defined in the mapping
	 * of the parent Issue.
	 * 
	 * @param parentIssueId
	 *            : the parent issue id.
	 * @return true if the project of the child issue is defined.
	 * @throws LinkNewIssueOperationException
	 *             : the exception.
	 */
	private boolean isProjectChildDefined(final String parentIssueId)
	throws LinkNewIssueOperationException {
		String childIssueProjectId = LinkedIssueContextManager.getInstance()
		.getProjectDefinedForChildIssueInMappingOfParentIssue(
				getIssueById(Long.valueOf(parentIssueId)), mappingid);
		boolean isChildProjectDefined = !(childIssueProjectId == null);
		return (isChildProjectDefined);
	}


	/**
	 * populate fields value with values inherit from the parent issue.
	 * 
	 * @param parentIssue
	 *            : parent issue.
	 * @throws LinkNewIssueOperationException
	 *             : LinkNewIssueOperationException.
	 * @throws RemoveException
	 *             : RemoveException.
	 * @throws EntityNotFoundException
	 *             : EntityNotFoundException.
	 */
	private void populateFieldHolderWithInheritValues(final Issue parentIssue,
			boolean inheritance) throws LinkNewIssueOperationException,
			RemoveException, EntityNotFoundException {

		LOG.debug("Populate fields of this new issue FieldHolderWithInheritValues");
		boolean hasSpecificFieldMapping = false;

		hasSpecificFieldMapping = LinkedIssueContextManager.getInstance()
		.isSpecificMappingActivated(lnioConfig, mappingid);
		LOG.debug("A specific field mapping should overwrite the population of fields : "
				+ hasSpecificFieldMapping);

		if (hasSpecificFieldMapping) {
			contextBean = LinkedIssueContextManager.getInstance()
			.getContextObjectById(mappingid, lnioConfig);
		}
		for (Iterator iterator = getFieldScreenRenderer()
				.getFieldScreenRenderTabs().iterator(); iterator.hasNext();) {
			FieldScreenRenderTab fieldScreenRenderTab = (FieldScreenRenderTab) iterator
			.next();
			for (Iterator iterator1 = fieldScreenRenderTab
					.getFieldScreenRenderLayoutItems().iterator(); iterator1
					.hasNext();) {
				FieldScreenRenderLayoutItem fieldScreenRenderLayoutItem = (FieldScreenRenderLayoutItem) iterator1
				.next();
				if (fieldScreenRenderLayoutItem.isShow(getIssueObject())) {
					boolean isFieldMapped = LinkedIssueContextManager
					.getInstance().isFieldSpecificMapped(
							lnioConfig,
							mappingid,
							fieldScreenRenderLayoutItem
							.getOrderableField().getId());
					populate(fieldScreenRenderLayoutItem,
							getFieldValuesHolder(), parentIssue,
							hasSpecificFieldMapping, isFieldMapped, inheritance);
					LOG.debug("LE KEYSET : " + getFieldValuesHolder().keySet());
				}
			}
		}
	}

	/**
	 * Populate a field function of different policies.
	 * 
	 * @param fieldScrRenderLayItem
	 * @param cFValuesHolder
	 * @param parentIssue
	 * @param hasSpecificFieldMapping
	 * @param isFieldMapped
	 * @param inheritance
	 * @throws LinkNewIssueOperationException
	 */
	private void populate(FieldScreenRenderLayoutItem fieldScrRenderLayItem,
			Map cFValuesHolder, Issue parentIssue,
			boolean hasSpecificFieldMapping, boolean isFieldMapped,
			boolean inheritance) throws LinkNewIssueOperationException {
		OrderableField field = fieldScrRenderLayItem.getOrderableField();
		if (isSkippedField(parentIssue, fieldScrRenderLayItem)) {
			LOG.debug("The field " + field.getId() + " has been skipped");
			fieldScrRenderLayItem.populateDefaults(
					getFieldValuesHolder(), getIssueObject());
		} else {
			if (hasSpecificFieldMapping && isFieldMapped) {
				LOG.debug("The field " + field.getId() + " has been populated by specific.");
				populateCustomfieldsWithSpecificMapping(field,
						getFieldValuesHolder(), parentIssue);
			} else {
				if (inheritance) {
					LOG.debug("The field " + field.getId() + " has been populated by heritage.");
					fieldScrRenderLayItem.populateFromIssue(
							getFieldValuesHolder(), parentIssue);
				} else {
					fieldScrRenderLayItem.populateDefaults(
							getFieldValuesHolder(), getIssueObject());
				}
			}
		}
	}

	/**
	 * Populate a field by a value due to a specific mapping.
	 * 
	 * @param field
	 * @param customFieldValuesHolder
	 * @param parentIssue
	 */
	private void populateCustomfieldsWithSpecificMapping(OrderableField field,
			Map customFieldValuesHolder, Issue parentIssue) {
		customFieldValuesHolder.put(field.getId(), getCustomFieldValueFromParentWithMapping(field.getId(), parentIssue) );


	}

	/**
	 * Retrieve the Field Value to set depending of a specific mapping.
	 * 
	 * @param fieldId
	 * @param parentIssue
	 * @return
	 */
	private Object getCustomFieldValueFromParentWithMapping(String fieldId,
			Issue parentIssue) {
		Object valueField = null;
		// retrieve the current context
		if (contextBean != null) {
			List listOfMappedFields = contextBean.getListOfFieldsMapping();
			if (listOfMappedFields != null && listOfMappedFields.size() > 0) {
				// update all fields values.
				for (int i = 0; i < listOfMappedFields.size(); i++) {
					LnioMappingOfFieldBean mappingOfFields = (LnioMappingOfFieldBean) listOfMappedFields
					.get(i);
					if (fieldId != null	&& fieldId.equals(mappingOfFields
							.getDestinationFieldId())) {
						valueField = getFieldValue(parentIssue, mappingOfFields
								.getOriginFieldId(), fieldId);
						return valueField;
					}
				}
			}
		}
		return valueField;
	}

	// ------ Utilities -----------------

	/**
	 * Checks if the field fieldScreenRenderLayoutItem.getOrderableField() have
	 * to be skipped.
	 * 
	 * @param parentIssue
	 *            : issue parent.
	 * @param fieldScreenRenderLayoutItem
	 *            : layout of rendering.
	 * @return true if the field should be skipped.
	 * @throws LinkNewIssueOperationException
	 *             : LinkNewIssueOperationException.
	 */
	public final boolean isSkippedField(final Issue parentIssue,
			final FieldScreenRenderLayoutItem fieldScreenRenderLayoutItem)
	throws LinkNewIssueOperationException {
		boolean toSkip = false;
		SkippedFieldsItem[] skippedFieldsItems = LinkedIssueContextManager
		.getInstance().getSkippedFieldsItems(parentIssue, mappingid);
		if (skippedFieldsItems != null && skippedFieldsItems.length > 0) {
			for (int i = 0; i < skippedFieldsItems.length; i++) {
				com.valiantys.jira.plugins.createissueandlink.lnioCastor.Field field = skippedFieldsItems[i]
				                                                                                          .getField();
				String id = LinkedIssueContextManager.getInstance()
				.getSkippedFieldId(field);
				if (id != null && id.length() > 0) {
					if (fieldScreenRenderLayoutItem.getOrderableField().getId()
							.equals("customfield_" + id)) {
						toSkip = true;
					}
				} else {
					String name = LinkedIssueContextManager.getInstance()
					.getSkippedFieldName(field);
					if (name != null && name.length() > 0) {
						if (fieldScreenRenderLayoutItem.getOrderableField()
								.getId().equals(name)) {
							toSkip = true;
						}
					}
				}
			}
		}
		return toSkip;
	}

	/**
	 * Retrieve the issue object by by the id.
	 * 
	 * @param parentId
	 *            : id of the parent issue.
	 * @return the parent issue.
	 */
	private Issue getIssueById(final Long parentId) {
		Issue parentIssue = null;
		if (parentId != null) {
			parentIssue = issueManager.getIssueObject(parentId);
		}
		return parentIssue;
	}

	/**
	 * get the value of the field populate by a specific mapping.
	 * @param issueSource
	 * @param fieldKeySource
	 * @param fieldKeyTarget
	 * @return
	 */
	private Object getFieldValue(final Issue issueSource, final String fieldKeySource, final String fieldKeyTarget) {

		Field fieldToUpdate = (Field) getFieldFromKey(fieldKeyTarget);
		Field fieldSource = (Field) getFieldFromKey(fieldKeySource);

		FieldManager fldManager = ManagerFactory.getFieldManager();

		Object newValue = null;

		// Case custom field fieldToUpdate
		if (fldManager.isCustomField(fieldToUpdate)) {

			CustomFieldParamsImpl valueCF = new CustomFieldParamsImpl();

			// Case custom field fieldSource
			if (fldManager.isCustomField(fieldSource)) {
				Object cfValue = issueSource.getCustomFieldValue((CustomField) fieldSource);
				OutlookDate outlookDate = outlookDateManager.getOutlookDate(authenticationContext.getLocale());

				if (((CustomField) fieldSource).getCustomFieldType() instanceof DateCFType) {
					newValue = outlookDate.formatDatePicker((Date) cfValue);
				}else if(((CustomField) fieldSource).getCustomFieldType() instanceof DateTimeCFType) {
					newValue = outlookDate.formatDateTimePicker((Date) cfValue);
				}else{
					if (cfValue != null) {
						newValue = getCustomFieldValue(cfValue);
					}
				}

			} else {
				newValue = getFieldValue(fieldSource, issueSource);
			}

			List customFieldParams = new ArrayList();

			if (newValue instanceof ArrayList) {

				ArrayList l = (ArrayList) newValue;
				for (int i = 0; i < l.size(); i++) {
					customFieldParams.add(getCustomFieldValue(l.get(i)));
				}
			} else if (newValue instanceof CustomFieldParamsImpl) {
				CustomFieldParamsImpl cfpi = (CustomFieldParamsImpl) newValue;
				cfpi.transformObjectsToStrings();
				return cfpi;
			} else {
				customFieldParams.add(newValue);
			}
			valueCF.addValue(null, customFieldParams);
			return valueCF;

		} else {
			// retrieve the new value.
			if (fldManager.isCustomField(fieldSource)) {
				newValue = getCustomFieldValue(issueSource
						.getCustomFieldValue((CustomField) fieldSource));
			} else {
				newValue = getFieldValue(fieldSource, issueSource);
			}
			return newValue;

		}

	}


	/**
	 * Load the SLA configuration from the database and create object SLAConfig
	 * to represents the configuration.<br>
	 * 
	 * @return instance of SLAConfig object.
	 */
	private LnioFieldsMappingConfiguration loadLNIOConf() {
		try {
			lnioConfig = lnioConfigManager
			.getLnioFieldsMappingConfigurationObject();
		} catch (LinkNewIssueOperationException e) {
			e.printStackTrace();
			LOG.error("Failed to load LNIO configuration");
		}
		return lnioConfig;
	}

	/**
	 * Retrieve the custom field form a key.
	 * 
	 * @param key
	 *            : the key of the customfield.
	 * @return a Field object from given key. (Field or Custom Field).
	 */
	private Field getFieldFromKey(final String key) {
		FieldManager fieldManager = ManagerFactory.getFieldManager();
		Field field = null;
		if (fieldManager.isCustomField(key)) {
			field = fieldManager.getCustomField(key);
		} else {
			field = fieldManager.getField(key);
		}
		if (field == null) {
			throw new IllegalArgumentException("Unable to find field : " + key);
		}
		return field;
	}


	// ------ Conversion between fields values and value need to be displayed -----------------	

	/**
	 * The the field value of an issue.
	 * 
	 * @param fieldToUpdate
	 *            : the field to update of the issue.
	 * @param issue
	 *            : the issue.
	 * @return the value of the field.
	 */
	private Object getFieldValue(final Field fieldToUpdate, final Issue issue) {
		Object value = null;
		FieldManager fldManager = ManagerFactory.getFieldManager();

		if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.ASSIGNEE))) {
			if (issue.getAssignee() != null) {
				value = issue.getAssignee().getName();
			}
		} else if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.DESCRIPTION))) {
			value = issue.getDescription();
		} else if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.ENVIRONMENT))) {
			value = issue.getEnvironment();
		} else if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.PRIORITY))) {
			if (issue.getPriority() != null) {
				value = issue.getPriorityObject().getId();
			}
		} else if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.RESOLUTION))) {
			if (issue.getResolution() != null) {
				value = issue.getResolutionObject().getId();
			}
		} else if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.SUMMARY))) {
			value = issue.getSummary();
		} else if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.TIME_ESTIMATE))) {
			value = String.valueOf(issue.getEstimate());
		} else if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.TIME_SPENT))) {
			value = String.valueOf(issue.getTimeSpent());
		} else if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.REPORTER))) {
			if (issue.getReporter() != null) {
				value = issue.getReporter().getName();
			}
		} else if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.SECURITY))) {
			if (issue.getSecurityLevel() != null) {
				value = issue.getSecurityLevel().getEntityName();
			}
		} else if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.FIX_FOR_VERSIONS))) {
			if (issue.getFixVersions() != null) {
				value = issue.getFixVersions();
			}
		} else if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.AFFECTED_VERSIONS))) {
			if (issue.getAffectedVersions() != null) {
				value = issue.getAffectedVersions();
			}
		} else if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.DUE_DATE))) {
			if (issue.getDueDate() != null) {
				OutlookDate outlookDate = outlookDateManager
				.getOutlookDate(authenticationContext.getLocale());
				value = outlookDate.format(new Date(issue.getDueDate()
						.getTime()));
			}
		} else if (fieldToUpdate.equals(fldManager
				.getField(IssueFieldConstants.COMPONENTS))) {
			if (issue.getComponents() != null) {
				value = issue.getComponents();
			}
		}


		LOG.debug("value : " + value);
		return value;
	}

	/**
	 * The displayable (in form ) value of a custom field value.
	 */
	private Object getCustomFieldValue(Object customfield) {

		LOG.debug("Classe du customfield : " + customfield.getClass());

		if (customfield instanceof User) {
			return ((User) customfield).getName();
		} else if (customfield instanceof Group) {
			return ((Group) customfield).getName();
		} else if (customfield instanceof Project) {
			return ((Project) customfield).getName();
		} else if (customfield instanceof ProjectRole) {
			return ((ProjectRole) customfield).getName();
		} else if (customfield instanceof String) {
			return customfield.toString();
		} else if (customfield instanceof Date) {
			OutlookDate outlookDate = outlookDateManager.getOutlookDate(authenticationContext.getLocale());
			return outlookDate.format((Date) customfield);
		} else if (customfield instanceof Timestamp) {
			OutlookDate outlookDate = outlookDateManager.getOutlookDate(authenticationContext.getLocale());
			return outlookDate.format(new Date(((Timestamp) customfield).getTime()));
		} else if (customfield instanceof Double) {
			return new Float( ((Double) customfield).floatValue() ).toString();

		} else {
			return customfield;
		}
	}

	// ------ Getters & Setters & Helper Methods -----------------

	/**
	 * @return
	 */
	public Issue getOriginalIssue() {
		return originalIssue;
	}

	/**
	 * @param originalIssue
	 */
	public void setOriginalIssue(final Issue originalIssue) {
		this.originalIssue = originalIssue;
	}

	/**
	 * @return
	 */
	public String getLinkTypeName() {
		return new String(linkTypeName);
	}

	/**
	 * @param linkType
	 */
	public void setLinkTypeName(final String linkType) {
		this.linkTypeName = new String(linkType);
	}

	/**
	 * @return
	 */
	public String getLinkDesc() {
		return linkDesc;
	}

	/**
	 * @param linkDesc
	 */
	public void setLinkDesc(final String linkDesc) {
		this.linkDesc = linkDesc;
	}

	public String getParentid() {
		return parentid;
	}

	public void setParentid(String parentid) {
		this.parentid = parentid;
	}

	public Long getParentIssueId() {
		return parentIssueId;
	}

	public void setParentIssueId(String parentid) {
		this.parentIssueId = new Long(parentid);
	}

	public String getMappingid() {
		return mappingid;
	}

	public void setMappingid(String mappingid) {
		this.mappingid = mappingid;
	}

	public Collection getLinkDescs() {
		if (linkDescs == null) {
			Collection linkTypes = issueLinkTypeManager.getIssueLinkTypes();
			linkDescs = new ArrayList();
			IssueLinkType issueLinkType;
			for (Iterator iterator = linkTypes.iterator(); iterator.hasNext(); linkDescs
			.add(issueLinkType.getInward())) {
				issueLinkType = (IssueLinkType) iterator.next();
				linkDescs.add(issueLinkType.getOutward());
			}
		}
		return linkDescs;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
