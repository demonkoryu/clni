package com.valiantys.jira.plugins.createissueandlink.service;

import java.util.List;
import java.util.Map;

import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.valiantys.jira.plugins.createissueandlink.LinkNewIssueOperationException;
import com.valiantys.jira.plugins.createissueandlink.util.SynchronizationType;

/**
 * Service used to synchroniez issues. The synchronization is based on links
 * between issues. There are type of synchro:<br/>
 * - Fields mappings - Addition of comments
 * 
 * @author Maxime Cojan
 * 
 */
public interface SynchronizeIssuesService {

    /**
     * Synchronize the fields of the origin issue with all the issues list in
     * the listOfSyncrho
     * 
     * @param origin
     * @param synchronizationType
     * @param listOfSynchro
     */
    public void launchASynchronizationOnThisIssue(Issue origin, SynchronizationType synchronizationType, List<Map<String, Object>> listOfSynchro, IssueEvent event);

    /**
     * Synchronize the comments of the origin issue with all the issues list in
     * the listOfSyncrho
     * 
     * @param origin
     * @param synchronizationType
     * @param listOfSynchro
     * @param aComment
     */
    public void launchASynchronizationOnThisIssue(Issue origin, SynchronizationType synchronizationType, List<Map<String, Object>> listOfSynchro, Comment aComment, IssueEvent event);

    /**
     * get the list of issues to syncrhonize with a configured mapping for an
     * issue (origin in configuration)
     * 
     * @param origin
     * @return
     * @throws LinkNewIssueOperationException
     */
    public List<Map<String, Object>> getConfiguredSynchronizationsByParentIssue(Issue origin) throws LinkNewIssueOperationException;

    /**
     * Get the list of issues to syncrhonize with a configured mapping for an
     * issue (destination in configuration)
     * 
     * @param destination
     * @return
     * @throws LinkNewIssueOperationException
     */
    public List<Map<String, Object>> getConfiguredSynchronizationsByChildIssue(Issue destination) throws LinkNewIssueOperationException;

    /**
     * Get the list of issues to syncrhonize with a configured mapping for an
     * issue (destination and origin in configuration)
     * 
     * @param issue
     * @return
     * @throws LinkNewIssueOperationException
     */
    public List<Map<String, Object>> getAllSynchronizationsToDoByIssue(Issue issue) throws LinkNewIssueOperationException;

}
