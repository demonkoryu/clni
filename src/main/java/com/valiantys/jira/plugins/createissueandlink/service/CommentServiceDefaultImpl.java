package com.valiantys.jira.plugins.createissueandlink.service;

import org.apache.log4j.Logger;

import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;

/**
 * This service is usefull to syncrhonize comment from an origin issue to a
 * destination issue. For the moment only the addition of comment is taken into
 * account.
 * 
 * @author Maxime Cojan
 * 
 */
public class CommentServiceDefaultImpl implements CommentService {

    /**
     * Logger.
     */
    private static final Logger LOG = Logger.getLogger(CommentServiceDefaultImpl.class);

    /**
     * Issue Manager.
     */
    private final IssueManager issueManager;

    /**
     * Comment Manager.
     */
    private final CommentManager commentManager;

    /**
     * constructor
     * 
     * @param commentManager
     * @param issueManager
     */
    public CommentServiceDefaultImpl(final CommentManager commentManager, final IssueManager issueManager) {
	this.commentManager = commentManager;
	this.issueManager = issueManager;
    }

    /**
     * {@inheritDoc}
     */
    public void addCommentSynchronization(Long destinationId, Comment aComment) {

	MutableIssue destinationIssue = issueManager.getIssueObject(destinationId);

	commentManager.create(destinationIssue, aComment.getAuthor(), aComment.getBody(), false);
    }

}
