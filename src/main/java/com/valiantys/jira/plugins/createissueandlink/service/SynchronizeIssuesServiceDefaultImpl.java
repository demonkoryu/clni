package com.valiantys.jira.plugins.createissueandlink.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.atlassian.event.EventManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.valiantys.jira.plugins.createissueandlink.LinkNewIssueOperationException;
import com.valiantys.jira.plugins.createissueandlink.business.SynchronizationConfigBean;
import com.valiantys.jira.plugins.createissueandlink.listener.SynchronizeIssuesListener;
import com.valiantys.jira.plugins.createissueandlink.manager.LnioConfigurationManager;
import com.valiantys.jira.plugins.createissueandlink.util.SynchronizationType;

/**
 * Default implementation of the SynchronizeIssuesService
 * 
 * @author Maxime Cojan
 * 
 */
public class SynchronizeIssuesServiceDefaultImpl implements SynchronizeIssuesService {

    /**
     * class Logger.
     */
    private static final Logger LOG = Logger.getLogger(SynchronizeIssuesListener.class);

    /**
     * Manager of the configuration mapping and syncrho.
     */
    private final LnioConfigurationManager lnioConfigManager;

    /**
     * Manager of the Link type.
     */
    private final IssueLinkTypeManager issueLinkTypeManager;

    /**
     * Manager of the issue Link.
     */
    private final IssueLinkManager issueLinkManager;

    /**
     * Manager of issues.
     */
    private final IssueManager issueManager;

    /**
     * Service for fields synchronization.
     */
    private final FieldsMappingService fieldsMappingService;

    /**
     * Service for comments synchronization.
     */
    private final CommentService commentService;

    /**
     * Manger of the index
     */
    private final IssueIndexManager indexManager;

    private final EventPublisher eventPublisher;

    /**
     * KEY ORIGIN for the map of available synchronization
     */
    private static final String ORIGIN_ID = "ORIGIN_ID";

    /**
     * KEY DESTINATION for the map of available synchronization
     */
    private static final String DEST_ID = "DEST_ID";

    /**
     * KEY Mapping for the map of available synchronization
     */
    private static final String MAPPING_ID = "MAPPING_ID";

    /**
     * constructor
     * 
     * @param lnioConfigManager
     * @param issueLinkTypeManager
     * @param issueLinkManager
     * @param fieldsMappingService
     * @param issueManager
     * @param indexManager
     * @param commentService
     */
    public SynchronizeIssuesServiceDefaultImpl(final LnioConfigurationManager lnioConfigManager, final IssueLinkTypeManager issueLinkTypeManager, final IssueLinkManager issueLinkManager,
	    final FieldsMappingService fieldsMappingService, final IssueManager issueManager, final IssueIndexManager indexManager, final CommentService commentService,
	    final EventPublisher eventPublisher) {
	this.lnioConfigManager = lnioConfigManager;
	this.issueLinkTypeManager = issueLinkTypeManager;
	this.issueLinkManager = issueLinkManager;
	this.fieldsMappingService = fieldsMappingService;
	this.issueManager = issueManager;
	this.indexManager = indexManager;
	this.commentService = commentService;
	this.eventPublisher = eventPublisher;
    }

    /**
     * {@inheritDoc}
     */
    public void launchASynchronizationOnThisIssue(Issue origin, SynchronizationType synchronizationType, List<Map<String, Object>> listOfSynchro, IssueEvent event) {
	launchASynchronizationOnThisIssue(origin, synchronizationType, listOfSynchro, null, event);
    }

    /**
     * {@inheritDoc}
     */
    public void launchASynchronizationOnThisIssue(Issue origin, SynchronizationType synchronizationType, List<Map<String, Object>> listOfSynchro, Comment aComment, IssueEvent event) {

	LOG.debug("ORIGIN ISSUE KEY : " + origin.getKey());
	boolean hasbeenUpdated = false;
	for (Map<String, Object> map : listOfSynchro) {
	    LOG.debug("The synchro is launch");
	    Issue destination = issueManager.getIssueObject((Long) map.get(DEST_ID));
	    Issue originMapping = issueManager.getIssueObject((Long) map.get(ORIGIN_ID));
	    LOG.debug("MAPPING DESTINATION ISSUE KEY : " + destination.getKey());
	    LOG.debug("MAPPING ORIGIN ISSUE KEY : " + originMapping.getKey());
	    LOG.debug("MAPPING ID : " + map.get(MAPPING_ID));

	    hasbeenUpdated = false;

	    switch (synchronizationType) {
	    case COMMENTS:
		LOG.debug("COMMENT : " + aComment);
		LOG.debug("ORIGIN : " + origin.getKey() + " - DESTINATION : " + destination.getKey());

		if (origin.getKey().equals(destination.getKey())) {
		    // case reverse.
		    commentService.addCommentSynchronization((Long) map.get(ORIGIN_ID), aComment);
		} else {
		    commentService.addCommentSynchronization((Long) map.get(DEST_ID), aComment);
		}
		break;

	    case FIELDS:
		if (origin.getKey().equals(destination.getKey())) {
		    hasbeenUpdated = fieldsMappingService.doSynchronization((String) map.get(MAPPING_ID), (Long) map.get(ORIGIN_ID), (Long) map.get(DEST_ID), true);
		} else {
		    hasbeenUpdated = fieldsMappingService.doSynchronization((String) map.get(MAPPING_ID), (Long) map.get(DEST_ID), (Long) map.get(ORIGIN_ID), false);
		}

		try {

		    if (hasbeenUpdated) {
			indexManager.reIndex(destination);
			indexManager.reIndex(origin);
		    }
		    LOG.debug("Destination issue : " + destination.getKey() + " is reindexed...");
		} catch (IndexException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}

		break;
	    }

	}
    }

    /**
     * {@inheritDoc}
     */
    public List<Map<String, Object>> getConfiguredSynchronizationsByParentIssue(Issue origin) throws LinkNewIssueOperationException {
	List<Map<String, Object>> synchronizationToDo = null;

	// Get origin project id
	Long projetId = origin.getProjectObject().getId();
	LOG.debug("projetId : " + projetId);

	if (lnioConfigManager.isProjectSynchronizable(projetId)) {
	    // Get all outwardslinks
	    List<IssueLink> issueLinks = issueLinkManager.getOutwardLinks(origin.getId());
	    for (IssueLink issueLink : issueLinks) {
		Long issueIdDestination = issueLink.getDestinationId();
		LOG.debug("issueIdDestination : " + issueIdDestination);
		LOG.debug("issueLink Type Id : " + issueLink.getIssueLinkType().getId());
		SynchronizationConfigBean configSynchro = lnioConfigManager.getSynchronizationConfig(projetId, issueLink.getIssueLinkType().getId());
		if (configSynchro != null) {
		    Map<String, Object> aSynchro = new HashMap<String, Object>();
		    aSynchro.put(ORIGIN_ID, origin.getId());
		    aSynchro.put(DEST_ID, issueIdDestination);
		    aSynchro.put(MAPPING_ID, configSynchro.getFieldsMappingId());

		    if (synchronizationToDo == null) {
			synchronizationToDo = new ArrayList<Map<String, Object>>();
		    }
		    synchronizationToDo.add(aSynchro);
		}

	    }

	}
	return synchronizationToDo;

    }

    /**
     * {@inheritDoc}
     */
    public List<Map<String, Object>> getConfiguredSynchronizationsByChildIssue(Issue destination) throws LinkNewIssueOperationException {
	List<Map<String, Object>> synchronizationToDo = null;

	LOG.debug("destination Id : " + destination.getId());

	// Get all inwardslinks
	List<IssueLink> issueLinks = issueLinkManager.getInwardLinks(destination.getId());
	for (IssueLink issueLink : issueLinks) {
	    Long issueIdOrigin = issueLink.getSourceId();
	    Long linkTypeId = issueLink.getLinkTypeId();

	    LOG.debug("issueId Origin : " + issueIdOrigin);

	    Issue origin = issueManager.getIssueObject(issueIdOrigin);
	    // Get origin project id
	    Long projetId = origin.getProjectObject().getId();
	    LOG.debug("projetId : " + projetId);

	    if (lnioConfigManager.isProjectSynchronizable(projetId)) {
		LOG.debug("Synchro for project : " + lnioConfigManager.isProjectSynchronizable(projetId));
		SynchronizationConfigBean configSynchro = lnioConfigManager.getSynchronizationConfig(projetId, linkTypeId);
		if (configSynchro != null) {
		    Map<String, Object> aSynchro = new HashMap<String, Object>();
		    aSynchro.put(ORIGIN_ID, origin.getId());
		    aSynchro.put(DEST_ID, destination.getId());
		    aSynchro.put(MAPPING_ID, configSynchro.getFieldsMappingId());
		    if (synchronizationToDo == null) {
			synchronizationToDo = new ArrayList<Map<String, Object>>();
		    }
		    synchronizationToDo.add(aSynchro);
		}

	    }
	}

	return synchronizationToDo;

    }

    /**
     * {@inheritDoc}
     */
    public List<Map<String, Object>> getAllSynchronizationsToDoByIssue(Issue issue) throws LinkNewIssueOperationException {
	List<Map<String, Object>> synchronizationToDo = null;

	List<Map<String, Object>> outwardLinks = getConfiguredSynchronizationsByParentIssue(issue);
	List<Map<String, Object>> inwardLinks = getConfiguredSynchronizationsByChildIssue(issue);

	if (outwardLinks != null && outwardLinks.size() > 0) {
	    synchronizationToDo = outwardLinks;
	}

	if (inwardLinks != null && inwardLinks.size() > 0) {
	    if (synchronizationToDo != null && synchronizationToDo.size() > 0) {
		synchronizationToDo.addAll(inwardLinks);
	    } else {
		synchronizationToDo = inwardLinks;
	    }
	}
	return synchronizationToDo;
    }

}
