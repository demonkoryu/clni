<%@ taglib prefix="ww" uri="webwork" %>
<%@ taglib prefix="aui" uri="webwork" %>
<%@ taglib prefix="page" uri="sitemesh-page" %>
<%@ page import="com.atlassian.jira.ComponentManager" %>
<%@ page import="com.atlassian.jira.web.action.util.FieldsResourceIncluder" %>
<html>
<head>
    <ww:if test="ableToCreateIssueInSelectedProject == 'true'"><meta content="genericaction" name="decorator" /></ww:if>
    <ww:else><meta content="error" name="decorator" /></ww:else>
    <title><ww:text name="'createissue.title'"/></title>
    <content tag="section">find_link</content>
    <%
        final FieldsResourceIncluder fieldResourceIncluder = ComponentManager.getComponentInstanceOfType(FieldsResourceIncluder.class);
        fieldResourceIncluder.includeFieldResourcesForCurrentUser();
    %>
</head>
<body class="type-a">
<ww:if test="ableToCreateIssueInSelectedProject == 'true'">
    <div class="content intform">
        <page:applyDecorator id="issue-create" name="auiform">
            <page:param name="action">CreateIssueAndLinkDetails.jspa</page:param>
            <page:param name="submitButtonName">Create</page:param>
            <page:param name="submitButtonText"><ww:property value="submitButtonName" escape="false" /></page:param>
            <page:param name="cancelLinkURI"><ww:url value="'default.jsp'" atltoken="false"/></page:param>
            <page:param name="isMultipart">true</page:param>

            <aui:component template="formHeading.jsp" theme="'aui'">
                <aui:param name="'text'"><ww:text name="'createissue.title'"/></aui:param>
            </aui:component>

            <aui:component name="'pid'" template="hidden.jsp" theme="'aui'" />
            <aui:component name="'issuetype'" template="hidden.jsp" theme="'aui'" />

            <page:applyDecorator name="auifieldgroup">
                <aui:component id="'project-name'" label="text('issue.field.project')" name="'project/string('name')'" template="formFieldValue.jsp" theme="'aui'" />
            </page:applyDecorator>

            <page:applyDecorator name="auifieldgroup">
                <aui:component id="'issue-type'" label="text('issue.field.issuetype')" name="'issueType'" template="formIssueType.jsp" theme="'aui'">
                    <aui:param name="'issueType'" value="/constantsManager/issueType(issuetype)" />
                </aui:component>
            </page:applyDecorator>

            <ww:component template="issuefields.jsp" name="'createissue'">
                <ww:param name="'displayParams'" value="/displayParams"/>
                <ww:param name="'issue'" value="/issueObject"/>
                <ww:param name="'tabs'" value="/fieldScreenRenderTabs"/>
                <ww:param name="'errortabs'" value="/tabsWithErrors"/>
                <ww:param name="'selectedtab'" value="/selectedTab"/>
                <ww:param name="'ignorefields'" value="/ignoreFieldIds"/>
                <ww:param name="'create'" value="'true'"/>
            </ww:component>
			
			<aui:component name="'parentid'" template="hidden.jsp" theme="'aui'"/>
			<aui:component name="'mappingid'" template="hidden.jsp" theme="'aui'"/>
			
			<aui:component name="'linkTypeName'" template="hidden.jsp" theme="'aui'" /> 
			<aui:component name="'linkDirection'" template="hidden.jsp" theme="'aui'" />     
			
			<jsp:include page="/includes/panels/updateissue_comment.jsp" />

        </page:applyDecorator>
    </div>
</ww:if>
<ww:else>
    <page:applyDecorator name="auierrorpanel">
        <page:param name="title"><ww:text name="'panel.errors'"/></page:param>
		<%@ include file="/includes/createissue-notloggedin.jsp" %>
    </page:applyDecorator>
</ww:else>
</body>
</html>